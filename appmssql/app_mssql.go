package appmssql

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"database/sql"
	_ "github.com/denisenkom/go-mssqldb" // mssql драйвер
	"github.com/gorilla/mux"
)

// Model - основные столбцы в таблицах
type Model struct {
	ID        uint       `gorm:"column:ID;primary_key"`
	CreatedAt time.Time  `gorm:"column:CreatedAt"`
	UpdatedAt time.Time  `gorm:"column:UpdatedAt"`
	DeletedAt *time.Time `gorm:"column:DeletedAt"`
}

// Config - главная структура, в которой хранятся все настройки программы
type Config struct {
	Dbpb   *dbpb
	Router *mux.Router
	DB     *sql.DB
}
type dbpb struct {
	User   string `yaml:"user"`
	Pass   string `yaml:"pass"`
	Server string `yaml:"server"`
	DbName string `yaml:"dbname"`
	Port   int64  `yaml:"port"`
}

// Initialize - инициализация подключения к базе данных
func (a *Config) Initialize() error {
	connString := fmt.Sprintf("sqlserver://%s:%s@%s:%d?database=%s",
		a.Dbpb.User, a.Dbpb.Pass, a.Dbpb.Server, a.Dbpb.Port, a.Dbpb.DbName)

	var err error
	a.DB, err = sql.Open("sqlserver", connString)
	if err != nil {
		return err
	}
	return nil
}

// SetConnectVars - установка значений подключения
func (a *Config) SetConnectVars() error {

	port := getEnvFromKey(`DB_PORT`, `1433`)
	portout, err := strconv.ParseInt(port, 10, 64)
	a.Dbpb = &dbpb{
		getEnvFromKey(`DB_LOGIN`, `n.spirichev`),
		getEnvFromKey(`DB_PASS`, `***`),
		getEnvFromKey(`DB_SERVER`, `s-db-10.ollis`),
		getEnvFromKey(`DB_NAME`, `PortalBase`),
		portout,
	}
	if err != nil {
		return err
	}
	return nil
}

// getEnvFromKey - получение значения переменной окружения по заданному ключу
// key (string) - имя переменной
// defaultVal (string) - значение по-умолчанию, если переменная не найдется
func getEnvFromKey(key, defaultVal string) string {
	envVar := os.Getenv(key)
	if len(envVar) < 1 {
		return defaultVal
	}
	return envVar
}

// GetPort получение порта из переменной окружения среды
func GetPort() string {
	port := os.Getenv(`PORT`)
	if len(port) < 1 {
		return `5000`
	}
	return port
}
