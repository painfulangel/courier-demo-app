
#build stage
FROM golang:alpine AS builder
WORKDIR /go/src/gitlab.com/ollisclub/crm/delivery
COPY . .
RUN apk add --no-cache git
RUN go get -d -v ./...
#RUN go install -v ./...
#RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o appdelivery .
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o appdelivery .

#final stage
FROM alpine:latest
RUN apk --no-cache add ca-certificates
COPY --from=builder /go/src/gitlab.com/ollisclub/crm/delivery/appdelivery /app/appdelivery
ENTRYPOINT /app/appdelivery
LABEL Name=delivery Version=0.0.1
EXPOSE 5000
