package main

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"gitlab.com/ollisclub/crm/delivery/model"

	"github.com/gorilla/mux"
)

// TestHttpRequestMainPage - тестирование главной страницы
func TestHttpRequestMainPage(t *testing.T) {
	initRoute()

	req, _ := http.NewRequest("GET", "/", nil)
	response := executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)

	if body := response.Body.String(); body != "Ollis Delivery v"+AppVersion {
		t.Errorf("На главной странице не найдена строка с информацией о версии приложения %s", body)
	}
}

// TestCheckIssetStatuses - проверка наличия статусов
func TestCheckIssetStatuses(t *testing.T) {
	initRoute()
	Conf.Initialize()

	req, _ := http.NewRequest("GET", "/status", nil)
	req.Header.Set(`Garagenumber`, `1234`)
	response := executeRequest(req)

	checkResponseCode(t, http.StatusOK, response.Code)

	var statuses []model.Statuses
	json.Unmarshal(response.Body.Bytes(), &statuses)
	body := response.Body.String()

	if len(statuses) < 1 {
		t.Errorf("Не обнаружен список статусов! %s", body)
	}
}

func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()

	Conf.Router.ServeHTTP(rr, req)
	return rr
}
func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func initRoute() {
	Conf.SetConnectVars()
	Conf.Router = mux.NewRouter()
	InitializeRoutes(&Conf)
}