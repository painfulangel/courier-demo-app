package model

import (
	"github.com/jinzhu/gorm"
)

//Statuses - статусы заказа
type Statuses struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}

const (
	// StatusTable - таблица перечня статусов
	StatusTable = `cc_sprav_statuses_zakaza`
)

// GetStatuses - метод получения последнего статуса заказа
func (s *Statuses) GetStatuses(db *gorm.DB) *[]Statuses {
	stat := new([]Statuses)
	db.Table(StatusTable).Select("*").Scan(&stat)
	return stat
}
