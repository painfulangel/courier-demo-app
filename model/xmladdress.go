package model

import "encoding/xml"

type XmlStruct struct {
	XMLName xml.Name `xml:"КонтактнаяИнформация"`
	C       []number `xml:"Состав>Состав>ДопАдрЭл>Номер"`
	Subject string   `xml:"Состав>Состав>СубъектРФ"`
	Area    string   `xml:"Состав>Состав>СвРайМО>Район"`
	Town    string   `xml:"Состав>Состав>Город"`
	Street  string   `xml:"Состав>Состав>Улица"`
}
type number struct {
	Type  string `xml:"Тип,attr"`
	Value string `xml:"Значение,attr"`
}

type Address struct {
	Street, Town, Area string
	House, DopHouse    HouseObject
}

type HouseObject struct {
	Name, Number string
}

type HouseSubject struct {
	Name, Number string
}
