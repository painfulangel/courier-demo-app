// 17.10.2019 15:40 - Ollis Club
// Author: Spirichev Nikita <n.spirichev@ollis.ru>
//
// Структуры и методы бизнес-логики курьера
//

package model

import (
	"database/sql/driver"
	"errors"
	"time"

	"github.com/jinzhu/gorm"
)

// OllisDelivery - главная структура управления курьерами. Имеет зависимости с остальными таблицами
type OllisDelivery struct {
	ID           uint   `gorm:"column:ID;primary_key" json:"id"`
	CourierCode  string `gorm:"column:CourierCode" json:"courierCode"`
	GarageNumber string `gorm:"column:GarageNumber" json:"garageNumber"`
	DateStart    int64  `gorm:"column:DateStart" json:"dateStart"`
	DateEnd      int64  `gorm:"column:DateEnd" json:"dateEnd"`
	Model
}

// TableName - Сделаем название в верхнем регистре
func (OllisDelivery) TableName() string {
	return "OLLIS_DELIVERY"
}

// OllisDeliveryGeo - структура по управлению позиционирования курьера
type OllisDeliveryGeo struct {
	ID                uint    `gorm:"column:ID;primary_key" json:"id"`
	CourierDeliveryID uint    `gorm:"column:CourierDeliveryID" json:"courierDeliveryId"`
	GarageNumber      string  `gorm:"column:GarageNumber" json:"garageNumber"`
	Lat               float64 `gorm:"column:Lat" json:"lat"`
	Lng               float64 `gorm:"column:Lng" json:"lng"`
	Time              int64   `gorm:"column:Time" json:"time"`
	Model
}

// TableName - Сделаем название в верхнем регистре
func (OllisDeliveryGeo) TableName() string {
	return "OLLIS_DELIVERY_GEO"
}

// OllisDeliveryStatuses - статусы курьера
type OllisDeliveryStatuses struct {
	ID                uint `gorm:"column:ID;primary_key" json:"id"`
	OrderRowID        uint `gorm:"column:OrderRowID" json:"orderRowId"`
	CourierDeliveryID OllisDelivery
	StatusCourier     uint   `gorm:"column:StatusCourier" json:"statusCourier"`
	StatusOrder       uint   `gorm:"column:StatusOrder" json:"statusOrder"`
	Comment           string `gorm:"column:Comment" json:"comment"`
	Model
}

// TableName - Сделаем название в верхнем регистре
func (OllisDeliveryStatuses) TableName() string {
	return "OLLIS_DELIVERY_STATUSES"
}

// OllisDeliveryUnits - Управление подразделениями курьера.
// Курьер может состорять сразу в нескольких подразделениях
type OllisDeliveryUnits struct {
	ID                uint    `gorm:"column:ID;primary_key" json:"id"`
	CourierDeliveryID uint    `gorm:"column:CourierDeliveryID" json:"courierDeliveryId"`
	GarageNumber      string  `gorm:"column:GarageNumber" json:"garageNumber"`
	UnitID            uint    `gorm:"column:UnitID" json:"unitId"`
	DateStart         int64   `gorm:"column:DateStart" json:"dateStart"`
	DateEnd           int64   `gorm:"column:DateEnd" json:"dateEnd"`
	Active            uint    `gorm:"column:Active" json:"active"`
	Comment           string  `gorm:"column:Comment" json:"comment"`
	Lat               float64 `gorm:"column:Lat" json:"lng"`
	Lng               float64 `gorm:"column:Lng" json:"lat"`
	UnitName          string  `gorm:"-" json:"unitName"`
	Model
}

// TableName - Сделаем название в верхнем регистре
func (OllisDeliveryUnits) TableName() string {
	return "OLLIS_DELIVERY_UNITS"
}

// OllisDeliveryOrders - Управление заказами курьера.
// Курьер может добавлять несколько заказов
type OllisDeliveryOrders struct {
	ID                uint      `gorm:"column:ID;primary_key" json:"id"`
	CourierDeliveryID uint      `gorm:"column:CourierDeliveryID" json:"courierDeliveryId"`
	GarageNumber      string    `gorm:"column:GarageNumber" json:"garageNumber"`
	OrderID           string    `gorm:"column:OrderID" json:"orderId"`
	UnitID            uint      `gorm:"column:UnitID" json:"unitId"`
	DeliveryTime      NullTime  `gorm:"column:DeliveryTime;type:datetime" json:"deliveryTime"`
	OrderDate         NullTime  `gorm:"column:OrderDate;type:datetime" json:"orderDate"`
	Comment           string    `gorm:"column:Comment" json:"comment"`
	ClientName        string    `gorm:"column:ClientName" json:"clientName"`
	Status            uint      `gorm:"column:Status" json:"status"`
	Address           string    `gorm:"column:Address" json:"address"`
	AddressNavigator  string    `gorm:"column:AddressNavigator" json:"addressNavigator"`
	StreetGuid        string    `gorm:"column:street_guid" json:"street_guid"`
	Lat               float64   `json:"lat"`
	Lng               float64   `json:"lng"`
	FinalDistance     float64   `json:"finalDistance"`
	Source            uint      `gorm:"column:Source" json:"source"`
	Cancel            uint      `gorm:"column:Cancel" json:"cancel"`
	Complete          uint      `gorm:"column:Complete" json:"complete"`
	OrderInfo         OrderInfo `json:"orderInfo"`
	TokenScan         uint      `gorm:"-" json:"tokenScan"`
	Model
}

// TableName - Сделаем название в верхнем регистре
func (OllisDeliveryOrders) TableName() string {
	return "OLLIS_DELIVERY_ORDERS"
}

type OllisDeliverySummaryOrders struct {
	CompleteTotal, CancelTotal uint32
	OrdersList                 []OllisDeliveryOrders
	TokenScan                  uint   `gorm:"-" json:"tokenScan"`
	Text                       string `json:"text"`
}

// UnitInfo - информация о подразделении
type UnitInfo struct {
	UnitCode int    `json:"unitCode"`
	UnitName string `json:"unitName"`
}
type OrderInfo struct {
	StatusCCName  string    `json:"statusCCName"`
	DeliverTo     time.Time `json:"deliverTo"`
	NormalAddress string    `json:"normalAddress"`
}
type NullTime struct {
	Time  time.Time
	Valid bool // Valid is true if Time is not NULL
}

// Scan implements the Scanner interface.
func (nt *NullTime) Scan(value interface{}) error {
	nt.Time, nt.Valid = value.(time.Time)
	return nil
}

// Value implements the driver Valuer interface.
func (nt NullTime) Value() (driver.Value, error) {
	if !nt.Valid {
		return nil, nil
	}
	return nt.Time, nil
}

const (
	// OrdersTable - таблица заказов
	OrdersTable = `cc_analyth_zakaz_clienta`
	// StatusListTable - таблица с перечислением статусов
	StatusListTable = `cc_sprav_statuses_zakaza`
)

func getDeliverys(db *gorm.DB, start, count int) ([]OllisDeliveryGeo, error) {
	return nil, errors.New("Not implemented")
}
