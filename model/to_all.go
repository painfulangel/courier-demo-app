package model

import "time"

// Model - Сталбцы с датами create, update, delete записи
type Model struct {
	CreatedAt time.Time  `gorm:"column:CreatedAt" json:"createdAt"`
	UpdatedAt time.Time  `gorm:"column:UpdatedAt" json:"updatedAt"`
	DeletedAt *time.Time `gorm:"column:DeletedAt" json:"deletedAt"`
}
