// 17.10.2019 15:40 - Ollis Club
// Author: Spirichev Nikita <n.spirichev@ollis.ru>
//
// Вычисление позиционирования курьера и применение методов на основе его положения и объектов на карте
//

package model

import (
	"log"

	"github.com/jinzhu/gorm"
	geo "github.com/kellydunn/golang-geo"
	"gitlab.com/ollisclub/crm/delivery/app"
)

// Info - Основная структура для работы с данными
type Info struct {
	gn     string
	db     *gorm.DB
	orders *[]OllisDeliveryOrders
	units  OllisDeliveryUnits
}

// init - инициализация конфига для последующей работы с данными
// 	gn - Гаражный номер
// 	db - объект базы данных
func (i *Info) InitGeoEvent(orders *[]OllisDeliveryOrders, units OllisDeliveryUnits, gn string, db *gorm.DB) {
	// Предварительная проверка, если что-то пошло не так, завершаем работу с методом
	if len(gn) == 0 || db == nil || len(*orders) == 0 || units.Lat < 1 || units.Lng < 1 || units.UnitID < 1 {
		return
	}
	i.gn = gn
	i.db = db
	i.orders = orders
	i.units = units

	// все данные записаны, начнем работу с геопозиционированием
	i.GeoEvents()
}

// GeoEventsStart - поиск координаты курьера и сравнение её с координатой ресторана. Если курьер находится в отдалении
// на app.DistanceRestoran метров, то меняем статус курьера
func (i *Info) GeoEventsStart(order OllisDeliveryOrders, geoPoint OllisDeliveryGeo) {
	if geoPoint.Lng > 0 && geoPoint.Lat > 0 {
		p := geo.NewPoint(geoPoint.Lat, geoPoint.Lng)
		p2 := geo.NewPoint(i.units.Lat, i.units.Lng)
		// вычислить расстояние между точками в м
		dist := p.GreatCircleDistance(p2) * 1000
		// Если курьер дальше 100 метров от точки
		if dist > app.GetDistanceResoran() {
			errors := i.db.Model(&order).Update(map[string]interface{}{"Status": app.Statuses.OneTheRoad}).GetErrors()
			if len(errors) > 0 {
				for _, err := range errors {
					log.Println(`Error! GeoEventsStart - не удалось сохранить статус заказа:`, err)
				}
			}
			log.Println("Далеко от ресторана. СТАТУС поменялся на ДОСТАВЛЯЕТСЯ")
		}
	}
}

// GeoEventsClient - поиск координаты курьера и сравнение её с координатой клиента. Если курьер находится в близи
// от клиента на app.Statuses.OneTheRoad метров, то меняем статус курьера
func (i *Info) GeoEventsClient(order *OllisDeliveryOrders, geoPoint OllisDeliveryGeo) float64 {

	var dist float64
	if geoPoint.Lng > 0 && geoPoint.Lat > 0 {
		p := geo.NewPoint(geoPoint.Lat, geoPoint.Lng)
		p2 := geo.NewPoint(i.units.Lat, i.units.Lng)
		// вычислить расстояние между точками в м
		dist := p.GreatCircleDistance(p2) * 1000
		// Если курьер дальше 100 метров от точки
		if dist <= app.GetDistanceClient() {
			errors := i.db.Model(&order).Update(map[string]interface{}{"Status": app.Statuses.InAddress}).GetErrors()
			if len(errors) > 0 {
				for _, err := range errors {
					log.Println(`Error! GeoEventsStart - не удалось сохранить статус заказа возле клиента:`, err)
				}
			}
			log.Println("Близко к клиенту!. СТАТУС поменялся на НА АДРЕСЕ")
		}
	}
	return dist
}

// GeoEvents
func (i *Info) GeoEvents() {
	var geoPoint OllisDeliveryGeo
	// TODO: Можно хранить последнюю координату пользователя в Redis чтобы брать оттуда последнюю точку
	// Получим последнюю координату курьера
	i.db.Where("GarageNumber = ?", i.gn).Order("Time DESC").Limit(1).Find(&geoPoint)

	for _, order := range *i.orders {

		switch order.Status {
		// Проверим, был ли выд=ан заказ курьеру
		case app.Statuses.Given:
			// заказ курьеру выдан, поэтому мы должны проверять как далеко он от своего ресторана
			i.GeoEventsStart(order, geoPoint)
		case app.Statuses.OneTheRoad:
			// Если курьер в дороге, то мы проверяем его координату и сопостовляем её с координатой клиента
			i.GeoEventsClient(order, geoPoint)
		}

	}
}
