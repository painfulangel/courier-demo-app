package model

import (
	"encoding/xml"
	"fmt"
	"log"
	"strconv"
)

var Objects map[int]string

// ParseAddr - парсинг xml-ки
func ParseAddr(xmlStr string) Address {
	rawData := []byte(xmlStr)

	var addr Address
	var v XmlStruct
	err := xml.Unmarshal(rawData, &v)
	if err != nil {
		log.Fatal(err)
	}
	parseBuilding(v, &addr)
	getStreet(v, &addr)
	getTown(v, &addr)
	getArea(v, &addr)
	return addr
}

// GetStringFromAddress - получение готового адреса в строковом представлении
func GetStringFromAddress(addr Address) string {
	var address string
	if len(addr.Area) > 0 {
		address += addr.Area + `, `
	}
	if len(addr.Town) > 0 {
		address += addr.Town + `, `
	}
	if len(addr.Street) > 0 {
		address += addr.Street + `, `
	}
	if len(addr.House.Number) > 0 {
		address += fmt.Sprintf(`%s %s`, addr.House.Name, addr.House.Number)
	}
	if len(addr.DopHouse.Number) > 0 {
		address += fmt.Sprintf(`, %s %s`, addr.DopHouse.Name, addr.DopHouse.Number)
	}

	return address
}

// getArea - получение района/субьекта
func getArea(v XmlStruct, addr *Address) {
	var area string
	if len(v.Subject) > 0 {
		area += v.Subject
	}
	if len(v.Area) > 0 {
		area += `, ` + v.Area
	}
	addr.Area = area
}

func getStreet(v XmlStruct, addr *Address) {
	addr.Street = v.Street
}

func getTown(v XmlStruct, addr *Address) {
	addr.Town = v.Town
}

func parseBuilding(v XmlStruct, addr *Address) {
	Objects := make(map[int]string)
	Objects[1010] = `Дом`
	Objects[1020] = `Дом`
	Objects[1030] = `Дом`
	Objects[1040] = `Участок`
	Objects[1050] = `корп.`
	Objects[1060] = `строение`
	Objects[1070] = `сооружение`
	Objects[1080] = `литера`
	for _, element := range v.C {
		// приведем тип объекта в число
		typeBuild, err := strconv.Atoi(element.Type)

		// Если не удалось проеобразовать, найдем вручную
		if err != nil {
			if element.Type == `1010` || element.Type == `1020` || element.Type == `1030` {
				addr.House.Number = element.Value
				addr.House.Name = `Дом №`
			}
			if element.Type == `1040` || element.Type == `1050` || element.Type == `1060` || element.Type == `1070` || element.Type == `1080` {
				addr.DopHouse.Number = element.Value
				addr.DopHouse.Name = `/`
			}
		} else {
			if typeBuild >= 1010 && typeBuild <= 1030 {
				addr.House.Number = element.Value
				addr.House.Name = Objects[typeBuild]
			} else if typeBuild >= 1040 && typeBuild <= 1080 {
				addr.DopHouse.Number = element.Value
				addr.DopHouse.Name = Objects[typeBuild]
			}
		}
	}
}
