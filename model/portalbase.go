// 17.10.2019 15:40 - Ollis Club
// Author: Spirichev Nikita <n.spirichev@ollis.ru>
//
// Структура таблиц заказа из КЦ и других, позволяющих получить основные данные
//

package model

import "time"

type CcAnalythZakazClienta struct {
	DateZakaza         NullTime  `gorm:"column:date_zakaza;type:datetime" json:"dateZakaza"`
	ZakazNumber        string    `gorm:"column:zakaz_number" json:"zakazNumber"`
	AddressDostavki    string    `gorm:"column:adres_dostavki" json:"addressDostavki"`
	StreetGuid         string    `gorm:"column:street_guid" json:"streetGuid"`
	AddressDostavkiXml string    `gorm:"column:adres_dostavki_znach_poley" json:"adresDostavkiZnachPoley"`
	VremyaDostavki     time.Time `gorm:"column:vremya_dostavki;type:datetime" json:"vremyaDostavki"`
	DateOtgruzki       time.Time `gorm:"column:date_otgruzki;type:datetime" json:"dateOtgruzki"`
	ZakazStatus        uint      `gorm:"column:zakaz_status" json:"zakazStatus"`
	StatusCallCenter   uint      `json:"statusCallCenter"`
	ZakazStatusDate    time.Time `gorm:"column:zakaz_status_date;type:datetime" json:"zakazStatusDate"`
	IsCanceled         uint8     `gorm:"column:is_cancel_ollis" json:"isCanceled"`
	Comment            string    `gorm:"column:dop_info_po_dostavke" json:"comment"`
	StatusCCName       string    `gorm:"column:StatusCCName" json:"statusCCName"`
	DeliverTo          NullTime  `gorm:"column:deliver_to" json:"deliverTo"`
}

type OllisStatuses struct {
	Code    int    `gorm:"column:code" json:"code"`
	Name    string `gorm:"column:name" json:"name"`
	Comment string `gorm:"column:comment" json:"comment"`
}
