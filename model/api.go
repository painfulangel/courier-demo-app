// 17.10.2019 15:40 - Ollis Club
// Author: Spirichev Nikita <n.spirichev@ollis.ru>
//
// Основной скрипт, обрабатывающий основную бизнес-логику
//

package model

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/ollisclub/crm/delivery/app"
	"gitlab.com/ollisclub/crm/delivery/fias"
)

// ResponseApp - шаблон ответа
type ResponseApp struct {
	Error []error         `json:"error"`
	Data  CourierResponce `json:"data"`
}

// CourierResponce - структура полной информаци о курьере для ответа
type CourierResponce struct {
	Status OllisDeliverySummaryOrders `json:"status"`
	Units  []OllisDeliveryUnits       `json:"units"`
	Orders []OllisDeliveryOrders      `json:"orders"`
}

type CourierResponce2 struct {
	Status string  `json:"status"`
	Units  []Units `json:"units"`
}
type Units struct {
	Unit   OllisDeliveryUnits    `json:"units"`
	Orders []OllisDeliveryOrders `json:"orders"`
}

// Points - координаты
type Points struct {
	Points []OllisDeliveryGeo `json:"points"`
}

// Points - координаты
type CourierUnit struct {
	UnitID int
}

type OrderControl struct {
	GarageNumber string `json:"garageNumber"`
	OrderID      string `json:"orderId"`
}

// GeoCoordinate - Структура координат
type GeoCoordinate struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

func (r *ResponseApp) GetAllInfoToday(GN string, db *gorm.DB) ResponseApp {
	var units []OllisDeliveryUnits
	var orders, applyOrders []OllisDeliveryOrders
	var orderCC CcAnalythZakazClienta
	var coordinates GeoCoordinate
	var statusApplyOrders OllisDeliverySummaryOrders

	coordinates = GeoCoordinate{0, 0}
	db.Select("[OLLIS_DELIVERY_UNITS].*, U.UnitName as unit_name, U.Lat, U.Lng").Joins("JOIN Ollis_UCHET_UNIT U ON U.UnitID = OLLIS_DELIVERY_UNITS.UnitID").Where("GarageNumber = ? AND DateStart > 0 AND DateEnd = 0", GN).Find(&units)
	db.Where("GarageNumber = ? AND Complete = 0 AND Cancel = 0", GN).Order("DeliveryTime", true).Find(&orders)

	// нам нужно проверить откуда мы получили заказ
	// если заказ получен от администратора или автоматом, то нужно подгрузить адрес
	// Адрес автоматом не подгружается, так как его нужно обработаоть
	if len(orders) > 0 {
		for key, order := range orders {
			//order.Source = 2 - источником данных является автоматическое добавление заказа через агент MSSQL
			if len(order.Address) < 1 && order.Source == 2 {
				db.Table("cc_analyth_zakaz_clienta T1").
					Select("convert(nvarchar(50), CAST(street_guid as uniqueidentifier)) as street_guid, zakaz_number, adres_dostavki, adres_dostavki_znach_poley, date_zakaza, zakaz_status, dop_info_po_dostavke, DATEADD(HOUR, DATEPART(HOUR,vremya_dostavki), (DATEADD(MINUTE, DATEPART(MINUTE,vremya_dostavki), date_otgruzki))) AS deliver_to").
					Where("zakaz_number = ?", order.OrderID).
					Scan(&orderCC)

				if len(orderCC.ZakazNumber) > 0 {
					if len(orderCC.AddressDostavkiXml) > 0 {
						address := ParseAddr(orderCC.AddressDostavkiXml)
						order.AddressNavigator = GetStringFromAddress(address)
						coordinates = GetCoordinates(orderCC.StreetGuid, address)
					} else {
						order.AddressNavigator = ``
					}
					order.Lat = coordinates.Lat
					order.Lng = coordinates.Lng
					order.Address = orderCC.AddressDostavki
					orders[key].Address = order.Address
					orders[key].AddressNavigator = order.AddressNavigator

					// после того как получили данные, обновим строку
					db.Model(&order).Updates(map[string]interface{}{
						"Status":           app.Statuses.Given,
						"Address":          order.Address,
						"AddressNavigator": order.AddressNavigator})

				}
			}
		}
	}

	// Обработка коордиднат курьера
	if len(orders) > 0 {
		geoEventStart(&orders, units[0], GN, db)
	}

	// Получим все завершенные заказы за текущий день
	db.Where("GarageNumber = ? AND (Complete > 0 OR Cancel > 0) AND DATEDIFF(day, CreatedAt, GETDATE()) = 0", GN).Find(&applyOrders)
	// Пройдемся по данным и инкрементируем все успешные и все отменённые заказы
	var completeOrd, canceledOrd uint32
	for _, value := range applyOrders {
		if value.Cancel > 0 {
			canceledOrd++
		}
		if value.Complete > 0 {
			completeOrd++
		}
	}
	// запишем полученные данные в структуру
	statusApplyOrders.CancelTotal = canceledOrd
	statusApplyOrders.CompleteTotal = completeOrd
	statusApplyOrders.OrdersList = applyOrders

	return ResponseApp{
		nil,
		CourierResponce{
			Status: statusApplyOrders,
			Units:  units,
			Orders: orders,
		},
	}
}

// geoEventStart - Инициализация и работа с функционалом геокоординат
func geoEventStart(orders *[]OllisDeliveryOrders, units OllisDeliveryUnits, gn string, db *gorm.DB) {
	var info Info
	info.InitGeoEvent(orders, units, gn, db)
}

// GetCoordinates - получение координат
func GetCoordinates(guidStr string, addr Address) GeoCoordinate {

	getGeo := fias.Geo{}

	// предопределим координаты
	coord := GeoCoordinate{0, 0}
	urlFias := `http://s-fias-1/@` + guidStr + `/houses/` + addr.House.Number + `;` + addr.DopHouse.Number + `;`
	log.Println(urlFias)
	r, err := http.Get(urlFias)

	// если что-то пошло не так, отдаем нулевые координаты
	if err != nil {
		log.Println(err)
	}
	if r.StatusCode > 400 {
		log.Println(`Адрес не найден: статус`, r.StatusCode)
	}
	defer r.Body.Close()

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Println(err)
	}

	if err := json.Unmarshal(body, &getGeo); err != nil {
		log.Println("Не найден")
		return coord
	} else {
		if getGeo.Lat > 0 && getGeo.Lng > 0 {
			coord.Lat = getGeo.Lat
			coord.Lng = getGeo.Lng
		}
	}
	return coord
}
