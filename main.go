// 17.10.2019 15:40 - Ollis Club
// Author: Spirichev Nikita <n.spirichev@ollis.ru>
//
// Главвный скрипт обрабатывающий GET/POST запросы
//

package main

import (
	"context"
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/jinzhu/gorm"
	geo "github.com/kellydunn/golang-geo"
	"gitlab.com/ollisclub/crm/delivery/app"
	"gitlab.com/ollisclub/crm/delivery/appmssql"
	"gitlab.com/ollisclub/crm/delivery/model"
	"go.uber.org/zap"
	"log"
	"net/http"
	"strconv"
	"time"
)

// AppVersion - версия сервиса
const AppVersion = `1.2.0`

// Conf инициализируем конфиг
var Conf app.Config

// ConfMSSQL - конфиг mssql
var ConfMssql appmssql.Config

var status model.Statuses

var responseModel *model.ResponseApp

// InitializeRoutes - инициализируем наши роуты
func InitializeRoutes(a *app.Config) {
	a.Router.HandleFunc("/", mainPage).Methods("GET")
	// Работа с заказами
	a.Router.HandleFunc("/order", getAllOrders).Methods("GET")

	// тест работы с датами
	a.Router.HandleFunc("/deliverytime", getDeliveryTime).Methods("GET")

	// Работа со сканером
	a.Router.HandleFunc("/scanunit", setCourierOnUnit).Methods("POST")
	a.Router.HandleFunc("/scanorder", setCourierOnOrder).Methods("POST")

	// Управление сменой
	a.Router.HandleFunc("/unitstop", setCourierOnUnitStop).Methods("POST")

	// Управление заказами
	a.Router.HandleFunc("/ordercomplete", setCourierOrderComplete).Methods("POST")
	a.Router.HandleFunc("/ordercancel", setCourierOrderCancel).Methods("POST")

	// Работа с позиционированием
	a.Router.HandleFunc("/geo", geoAdd).Methods("POST")

	// Работат со справочниками
	a.Router.HandleFunc("/status", getStatuses).Methods("GET")
	a.Router.HandleFunc("/status/{id:[0-9]+}", getStatusCourier).Methods("GET")
	a.Router.HandleFunc("/status/name/{word}", getStatusName).Methods("GET")

	// Системные функции
	a.Router.HandleFunc("/migration", migration).Methods("GET")
	a.Router.HandleFunc("/geotest", geoTest).Methods("GET")
	a.Router.HandleFunc("/geotest1000", geoTest1000).Methods("GET")
}

func geoTest1000(w http.ResponseWriter, r *http.Request) {
	var orderCC []model.CcAnalythZakazClienta

	Conf.DB.Table("cc_analyth_zakaz_clienta T1").
		Select("TOP 10000 convert(nvarchar(50), CAST(street_guid as uniqueidentifier)) as street_guid, zakaz_number, adres_dostavki, adres_dostavki_znach_poley, date_zakaza, zakaz_status, dop_info_po_dostavke, DATEADD(HOUR, DATEPART(HOUR,vremya_dostavki), (DATEADD(MINUTE, DATEPART(MINUTE,vremya_dostavki), date_otgruzki))) AS deliver_to").
		Order(`date_zakaza DESC`).
		Where("len(adres_dostavki_znach_poley) > 0 and id = 3890958").
		Scan(&orderCC)

	all := 0
	i := 0
	k := 0
	nk := 0

	for _, v := range orderCC {
		if len(v.ZakazNumber) > 0 {

			if len(v.AddressDostavkiXml) > 0 {
				i++
				// address - структура адреса
				address := model.ParseAddr(v.AddressDostavkiXml)
				coordinates := model.GetCoordinates(v.StreetGuid, address)
				if coordinates.Lng > 0 && coordinates.Lat > 0 {
					k++
					log.Println("Успех!! Адрес:", address, "координаты:", coordinates)
				} else {
					nk++
					log.Println("Не найдены координаты! Адрес:", address, v.AddressDostavki, "координаты:", coordinates)
				}
			} else {
				log.Println("Не найдены XML Адрес! Адрес:", v.AddressDostavki)
			}
		}
		all++
	}

	log.Println("Всего строк:", all)
	log.Println("Корректных строк:", i)
	log.Println("Найдены координаты:", k)
	log.Println("НЕ Найдены координаты:", nk)

}

func geoTest(w http.ResponseWriter, r *http.Request) {

	fmt.Println(time.Now().Format("20060102 15:04:05.999999999Z07:00"))

	addr := model.ParseAddr(`<КонтактнаяИнформация xmlns="http://www.v8.1c.ru/ssl/contactinfo" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Представление="Санкт-Петербург г, Дальневосточный пр-кт, дом № 73литА, квартира Офис"><Состав xsi:type="Адрес" Страна="РОССИЯ"><Состав xsi:type="АдресРФ"><СубъектРФ>Санкт-Петербург г</СубъектРФ><Улица>Дальневосточный пр-кт</Улица><ДопАдрЭл><Номер Тип="1010" Значение="73литА"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="2010" Значение="Офис"/></ДопАдрЭл><ДопАдрЭл ТипАдрЭл="10100000" Значение="190000"/></Состав></Состав></КонтактнаяИнформация>`)
	fmt.Println(model.GetStringFromAddress(addr))
	fmt.Println(`http://s-fias-1/@/houses/` + addr.House.Number + `;` + addr.DopHouse.Number + `;`)

	addr = model.ParseAddr(`<КонтактнаяИнформация xmlns="http://www.v8.1c.ru/ssl/contactinfo" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Представление="Санкт-Петербург г, Стахановцев ул, дом № д, квартира 414"><Состав xsi:type="Адрес" Страна="РОССИЯ"><Состав xsi:type="АдресРФ"><СубъектРФ>Санкт-Петербург г</СубъектРФ><Улица>Стахановцев ул</Улица><ДопАдрЭл><Номер Тип="1010" Значение="д"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="2010" Значение="414"/></ДопАдрЭл><ДопАдрЭл ТипАдрЭл="10100000" Значение="190000"/></Состав></Состав></КонтактнаяИнформация>`)
	fmt.Println(model.GetStringFromAddress(addr))
	fmt.Println(`http://s-fias-1/@/houses/` + addr.House.Number + `;` + addr.DopHouse.Number + `;`)

	addr = model.ParseAddr(`<КонтактнаяИнформация xmlns="http://www.v8.1c.ru/ssl/contactinfo" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Представление="Санкт-Петербург г,Стахановцев ул, дом № 14, литера А, корпус 1, квартира БЦ_&quot;Ладога-Центр&quot;"><Состав xsi:type="Адрес" Страна="РОССИЯ"><Состав xsi:type="АдресРФ"><СубъектРФ>Санкт-Петербург г</СубъектРФ><Улица>Стахановцев ул</Улица><ДопАдрЭл><Номер Тип="2010" Значение="БЦ_&quot;Ладога-Центр&quot;"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="1050" Значение="1"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="1080" Значение="А"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="1010" Значение="14"/></ДопАдрЭл></Состав></Состав></КонтактнаяИнформация>`)
	fmt.Println(model.GetStringFromAddress(addr))
	fmt.Println(`http://s-fias-1/@/houses/` + addr.House.Number + `;` + addr.DopHouse.Number + `;`)

	addr = model.ParseAddr(`<КонтактнаяИнформация xmlns="http://www.v8.1c.ru/ssl/contactinfo" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Представление="195220, Санкт-Петербург г, Бутлерова ул, дом № 11, корпус 4, квартира 164, этаж 5, код парадной 164"><Комментарий> / +7 (967) 5631855 / </Комментарий><Состав xsi:type="Адрес" Страна="РОССИЯ"><Состав xsi:type="АдресРФ"><СубъектРФ>Санкт-Петербург г</СубъектРФ><Улица>Бутлерова ул</Улица><ДопАдрЭл><Номер Тип="1050" Значение="4"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="1010" Значение="11"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="2010" Значение="164"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="2060" Значение="5"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="2070" Значение="164"/></ДопАдрЭл><ДопАдрЭл ТипАдрЭл="10100000" Значение="195220"/></Состав></Состав></КонтактнаяИнформация>`)
	fmt.Println(model.GetStringFromAddress(addr))
	fmt.Println(`http://s-fias-1/@/houses/` + addr.House.Number + `;` + addr.DopHouse.Number + `;`)

	addr = model.ParseAddr(`<КонтактнаяИнформация xmlns="http://www.v8.1c.ru/ssl/contactinfo" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Представление="195220, Санкт-Петербург г, Бутлерова ул, дом № 11, корпус 4, квартира 164, этаж 5, код парадной 164"><Комментарий> / +7 (967) 5631855 / </Комментарий><Состав xsi:type="Адрес" Страна="РОССИЯ"><Состав xsi:type="АдресРФ"><СубъектРФ>Санкт-Петербург г</СубъектРФ><Улица>Бутлерова ул</Улица><ДопАдрЭл><Номер Тип="1050" Значение="4"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="1010" Значение="11"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="2010" Значение="164"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="2060" Значение="5"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="2070" Значение="164"/></ДопАдрЭл><ДопАдрЭл ТипАдрЭл="10100000" Значение="195220"/></Состав></Состав></КонтактнаяИнформация>`)
	fmt.Println(model.GetStringFromAddress(addr))
	fmt.Println(`http://s-fias-1/@/houses/` + addr.House.Number + `;` + addr.DopHouse.Number + `;`)

	addr = model.ParseAddr(`<КонтактнаяИнформация xmlns="http://www.v8.1c.ru/ssl/contactinfo" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Представление="199178, Санкт-Петербург г, 18-я В.О. линия, дом № 19, квартира 35, этаж 4, код парадной 35, широта 59.935620306, долгота 30.267310515"><Комментарий>18 линия</Комментарий><Состав xsi:type="Адрес" Страна="РОССИЯ"><Состав xsi:type="АдресРФ"><СубъектРФ>Санкт-Петербург г</СубъектРФ><Улица>18-я В.О. линия</Улица><ДопАдрЭл><Номер Тип="1010" Значение="19"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="2010" Значение="35"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="2060" Значение="4"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="2070" Значение="35"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="2080" Значение="59.935620306"/></ДопАдрЭл><ДопАдрЭл><Номер Тип="2090" Значение="30.267310515"/></ДопАдрЭл><ДопАдрЭл ТипАдрЭл="10100000" Значение="199178"/></Состав></Состав></КонтактнаяИнформация>`)
	fmt.Println(model.GetStringFromAddress(addr))
	fmt.Println(`http://s-fias-1/@/houses/` + addr.House.Number + `;` + addr.DopHouse.Number + `;`)

	addr = model.ParseAddr(`<КонтактнаяИнформация xmlns="http://www.v8.1c.ru/ssl/contactinfo" xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" Представление="192177, Санкт-Петербург г, Прибрежная ул, дом № 18"><Комментарий/><Состав xsi:type="Адрес" Страна="РОССИЯ"><Состав xsi:type="АдресРФ"><СубъектРФ>Санкт-Петербург г</СубъектРФ><Округ/><СвРайМО><Район/></СвРайМО><Город/><НаселПункт/><Улица>Прибрежная ул</Улица><новаУлица>000012853</новаУлица><ОКТМО>40381000</ОКТМО><ДопАдрЭл ТипАдрЭл="10200000" Значение=""/><ДопАдрЭл ТипАдрЭл="10400000" Значение=""/><ДопАдрЭл><Номер Тип="1010" Значение="18"/></ДопАдрЭл><ДопАдрЭл ТипАдрЭл="10100000" Значение="192177"/></Состав></Состав></КонтактнаяИнформация>`)
	fmt.Println(model.GetStringFromAddress(addr))
	fmt.Println(`http://s-fias-1/@/houses/` + addr.House.Number + `;` + addr.DopHouse.Number + `;`)

	// Make a few points
	p := geo.NewPoint(60.018048, 30.243797)
	p2 := geo.NewPoint(60.030111, 30.330497)

	// вычислить расстояние между точками в км
	dist := p.GreatCircleDistance(p2)
	fmt.Println(dist)
}

// migration - миграция приложения
func migration(w http.ResponseWriter, r *http.Request) {
	Conf.DB.AutoMigrate(&model.OllisDeliveryGeo{})
	Conf.DB.AutoMigrate(&model.OllisDelivery{})
	Conf.DB.AutoMigrate(&model.OllisDeliveryStatuses{})
	Conf.DB.AutoMigrate(&model.OllisDeliveryUnits{})
	Conf.DB.AutoMigrate(&model.OllisDeliveryOrders{})
}

func getDeliveryTime(w http.ResponseWriter, r *http.Request) {
	order := new(model.CcAnalythZakazClienta)
	Conf.DB.Where("id = ?", 3715141).Find(&order)
	log.Println(order)
}

///
/// Работа с заказами
///
// setCourierOrderComplete - успешное завершение заказа
//
func setCourierOrderComplete(w http.ResponseWriter, r *http.Request) {

	var order model.OllisDeliveryOrders
	err := json.NewDecoder(r.Body).Decode(&order)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	order.GarageNumber = CheckGN(w, r)
	var count int
	//where := "GarageNumber = ? AND Cancel = 0 AND OrderID = ? AND Status = 0"
	// Если заказ уже взят, то его заново ее не создаем
	Conf.DB.Model(&order).Count(&count)
	if count > 0 {
		errors := Conf.DB.Model(&order).Update(map[string]interface{}{"Status": 14, "Complete": 1}).GetErrors()
		if len(errors) > 0 {
			for _, err := range errors {
				fmt.Println(err)
			}
		} else {
			_, err := AfterOrderEvent(int64(order.ID), 14)
			if err != nil {
				fmt.Println(err)
			}
		}

	}
	response := responseModel.GetAllInfoToday(CheckGN(w, r), Conf.DB)
	outputJSON(response, w, http.StatusCreated)
}

func setCourierOrderCancel(w http.ResponseWriter, r *http.Request) {

	var order model.OllisDeliveryOrders
	err := json.NewDecoder(r.Body).Decode(&order)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	order.GarageNumber = CheckGN(w, r)
	var count int
	//where := "GarageNumber = ? AND Cancel = 0 AND OrderID = ?"
	// Если заказ уже взят, то его заново ее не создаем
	Conf.DB.Model(&order).Count(&count)
	if count > 0 {
		errors := Conf.DB.Model(&order).Update(map[string]interface{}{"Status": 28, "Cancel": 1}).GetErrors()
		if len(errors) > 0 {
			for _, err := range errors {
				fmt.Println(err)
			}
		} else {
			_, err := AfterOrderEvent(int64(order.ID), 28)
			if err != nil {
				fmt.Println(err)
			}
		}
	}
	response := responseModel.GetAllInfoToday(CheckGN(w, r), Conf.DB)
	outputJSON(response, w, http.StatusCreated)
}

// setCourierOnOrder - Создание заказа курьера
func setCourierOnOrder(w http.ResponseWriter, r *http.Request) {
	var requestInfo string
	garageNumber := CheckGN(w, r)
	var order model.OllisDeliveryOrders
	var orderCC model.CcAnalythZakazClienta
	err := json.NewDecoder(r.Body).Decode(&order)

	if err != nil {
		http.Error(w, fmt.Sprintf(`Ошибка распознавания JSON данных! %s`, err.Error()), http.StatusInternalServerError)
		return
	}
	order.GarageNumber = garageNumber

	Conf.DB.Table("cc_analyth_zakaz_clienta T1").
		Select("street_guid, zakaz_number, adres_dostavki, adres_dostavki_znach_poley, date_zakaza, zakaz_status, dop_info_po_dostavke, DATEADD(HOUR, DATEPART(HOUR,vremya_dostavki), (DATEADD(MINUTE, DATEPART(MINUTE,vremya_dostavki), date_otgruzki))) AS deliver_to").
		Where("zakaz_number = ?", order.OrderID).
		Scan(&orderCC)

	if len(orderCC.ZakazNumber) > 0 {
		if len(orderCC.AddressDostavkiXml) > 0 {
			// address - структура адреса
			address := model.ParseAddr(orderCC.AddressDostavkiXml)
			order.AddressNavigator = model.GetStringFromAddress(address)
		} else {
			order.AddressNavigator = orderCC.AddressDostavki
		}
		order.Address = orderCC.AddressDostavki
		order.Comment = orderCC.Comment
		order.OrderDate = orderCC.DateZakaza
		order.DeliveryTime = orderCC.DeliverTo
	}

	var count int
	// Если заказ уже взят, то его заново не создаем
	ordersQuery := Conf.DB.Model(&model.OllisDeliveryOrders{}).Where("Cancel = 0 AND Complete = 0 AND OrderID = ?", order.OrderID).Count(&count)

	if ordersQuery.Error != nil {
		requestInfo = fmt.Sprintf(`%s%s`, requestInfo, ordersQuery.Error.Error())
	}
	if count == 0 {
		lastId, err := CreateMssqlOrder(&order)
		fmt.Println(lastId, err)
		if err != nil {
			requestInfo = fmt.Sprintf(`%s Заказ №%s Ошибка! %s`, requestInfo, order.OrderID, err.Error())
		}
	} else {
		requestInfo = fmt.Sprintf(`%s Заказ №%s уже взят курьером`, requestInfo, order.OrderID)
	}

	response := responseModel.GetAllInfoToday(CheckGN(w, r), Conf.DB)

	if order.TokenScan > 0 {
		response.Data.Status.TokenScan = order.TokenScan
	}
	response.Data.Status.Text = requestInfo

	outputJSON(response, w, http.StatusOK)
}

// checkSqlErrors - Обработчик MSSQL ошибок
func checkSqlErrors(name string, sqlError []error, db *gorm.DB, w http.ResponseWriter) bool {
	stringError := fmt.Sprintf(name, sqlError)
	if len(sqlError) > 0 {
		for _, errorEl := range sqlError {
			stringError += fmt.Sprintf(`%s`, errorEl)
		}
		db.Rollback()
		return false
	}
	return true
}

// setCourierOnOrderComplete - подтверждение заказа (курьер доставил заказ)
func setCourierOnOrderComplete(w http.ResponseWriter, r *http.Request) {
	garageNumber := CheckGN(w, r)
	var unit model.OllisDeliveryUnits

	unit.GarageNumber = garageNumber
	Conf.DB.Model(&unit).Where("GarageNumber = ? AND DateEnd = 0", garageNumber).Update("DateEnd", time.Now().Unix())

	response := responseModel.GetAllInfoToday(CheckGN(w, r), Conf.DB)
	outputJSON(response, w, http.StatusCreated)
}

// setCourierOnUnit - После сканирования QR-кода записываем курьера в базу
func setCourierOnUnit(w http.ResponseWriter, r *http.Request) {
	garageNumber := CheckGN(w, r)
	var unit model.OllisDeliveryUnits
	err := json.NewDecoder(r.Body).Decode(&unit)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	unit.GarageNumber = garageNumber

	var count int
	// Если смена уже открыта, то заново ее не создаем
	Conf.DB.Model(&model.OllisDeliveryUnits{}).Where("GarageNumber = ? AND DateEnd = 0 AND UnitID = ?", garageNumber, unit.UnitID).Count(&count)
	if count == 0 {
		lastId, err := CreateMssqlUnit(&unit)
		fmt.Println(lastId, err)
	}

	response := responseModel.GetAllInfoToday(CheckGN(w, r), Conf.DB)
	outputJSON(response, w, http.StatusCreated)
}

// setCourierOnUnitStop - закрыть смену курьера
func setCourierOnUnitStop(w http.ResponseWriter, r *http.Request) {
	garageNumber := CheckGN(w, r)
	var unit model.OllisDeliveryUnits

	err := json.NewDecoder(r.Body).Decode(&unit)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	unit.GarageNumber = garageNumber
	Conf.DB.Model(&unit).Update("DateEnd", time.Now().Unix())

	response := responseModel.GetAllInfoToday(CheckGN(w, r), Conf.DB)
	outputJSON(response, w, http.StatusCreated)
}

// getAllOrders - Получение всех заказов
func getAllOrders(w http.ResponseWriter, r *http.Request) {
	response := responseModel.GetAllInfoToday(CheckGN(w, r), Conf.DB)
	outputJSON(response, w, http.StatusCreated)
}
func geoAdd(w http.ResponseWriter, r *http.Request) {
	sugar := zap.NewExample().Sugar()
	defer sugar.Sync()

	sugar.Infof("Добавление координат")

	points := new(model.Points)
	err := json.NewDecoder(r.Body).Decode(&points)

	if err != nil {
		sugar.Infof("Добавление координат. Ошибка: %s", err.Error())
		fmt.Println(err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	addCoordinates(points)
	response := responseModel.GetAllInfoToday(CheckGN(w, r), Conf.DB)
	outputJSON(response, w, http.StatusCreated)
}

// getStatuses - получение всех статусов коллцентра
func getStatuses(w http.ResponseWriter, r *http.Request) {
	response := responseModel.GetAllInfoToday(CheckGN(w, r), Conf.DB)
	outputJSON(response, w, http.StatusCreated)
}

func getStatusCourier(w http.ResponseWriter, r *http.Request) {
	response := responseModel.GetAllInfoToday(CheckGN(w, r), Conf.DB)
	outputJSON(response, w, http.StatusCreated)
}

// getStatusID - поиск статуса по номеру
func getStatusID(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	word := vars["id"]
	statuses := new([]model.Statuses)

	num, _ := strconv.ParseInt(word, 10, 64)
	if err := Conf.DB.Table(model.StatusTable).Select("id, name").Where("id = ?", num).Scan(&statuses).Error; err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	outputJSON(statuses, w, http.StatusOK)
}

// getStatus
func getStatusName(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	word := vars["word"]
	statuses := new([]model.Statuses)

	if err := Conf.DB.Table(model.StatusTable).Select("id, name").Where("name like ?", `%`+word+`%`).Scan(&statuses).Error; err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	outputJSON(statuses, w, http.StatusOK)
}

// mainPage - Главная страница, которая отдает краткую информацию о микросервисе
func mainPage(w http.ResponseWriter, r *http.Request) {
	w.Write([]byte("Ollis Delivery v" + AppVersion))
}

// outputJson - вывод json массива в браузер
//
// name - данные которые хотим отобразить
//
// w - объект ответа
func outputJSON(name interface{}, w http.ResponseWriter, code int) {
	jsonVars, err := json.Marshal(name)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.Header().Set("Access-Control-Allow-Origin", "*")
	fmt.Println(w)
	w.WriteHeader(code)
	w.Write(jsonVars)
}

func CheckGN(w http.ResponseWriter, r *http.Request) string {
	garageNumber := r.Header.Get("Garagenumber")
	if garageNumber == "" {
		http.Error(w, "Ошибка! Не передан гаражный номер", http.StatusInternalServerError)
		return ""
	}
	return garageNumber
}

// CreateEmployee inserts an employee record
func CreateMssqlOrder2(GN *string, order *model.OllisDeliveryOrders) (int64, error) {
	ctx := context.Background()
	var err error

	tsql := `INSERT INTO OLLIS_DELIVERY_ORDERS (
	GarageNumber
	,OrderID
	,UnitID
	--,DeliveryTime
	,Status
	,Comment
	,ClientName
	,Address
	,Cancel
	,Complete
	) VALUES (
	@GarageNumber
	,@OrderID
	,0
	--,@DeliveryTime
	,@Statuss
	,@Commentt
	,@ClientName
	,@Address
	,@Cancel
	,@Complete); select convert(bigint, SCOPE_IDENTITY());`

	stmt, err := ConfMssql.DB.Prepare(tsql)
	if err != nil {
		return -1, err
	}
	defer stmt.Close()

	row := stmt.QueryRowContext(
		ctx,
		sql.Named("OrderID", order.OrderID),
		sql.Named("GarageNumber", GN))
	//sql.Named("DeliveryTime", order.DeliveryTime)
	sql.Named("Statuss", order.Status)
	sql.Named("Commentt", order.Comment)
	sql.Named("ClientName", order.ClientName)
	sql.Named("Address", order.Address)
	sql.Named("Cancel", order.Cancel)
	sql.Named("Complete", order.Complete)
	var newID int64
	err = row.Scan(&newID)
	if err != nil {
		return -1, err
	}

	return newID, nil
}

// CreateMssqlOrder добавление заказа
func CreateMssqlOrder(order *model.OllisDeliveryOrders) (int64, error) {
	ctx := context.Background()
	var err error

	tsql := fmt.Sprintf(`INSERT INTO OLLIS_DELIVERY_ORDERS (
	GarageNumber
	,OrderID
	,UnitID
	,DeliveryTime
	,Status
	,Comment
	,ClientName
	,Address
	,AddressNavigator
	,Cancel
	,Complete
	,CreatedAt
	,UpdatedAt
	) VALUES (
	'%s'
	,'%s'
	,0
	,'%s'
	,'%d'
	,'%s'
	,'%s'
	,'%s'
	,'%s'
	,'%d'
	,'%d'
	,'%s'
	,'%s'); select convert(bigint, SCOPE_IDENTITY());`,
		order.GarageNumber,
		order.OrderID,
		order.DeliveryTime.Time.Format("20060102 15:04:05"),
		//`20190101 21:33:33`,
		order.Status,
		order.Comment,
		order.ClientName,
		order.Address,
		order.AddressNavigator,
		order.Cancel,
		order.Complete,
		time.Now().Format("20060102 15:04:05.999999999Z07:00"),
		time.Now().Format("20060102 15:04:05.999999999Z07:00"))

	stmt, err := ConfMssql.DB.Prepare(tsql)
	log.Println(tsql)
	if err != nil {
		return -1, err
	}
	defer stmt.Close()

	row := stmt.QueryRowContext(ctx)
	var newID int64
	err = row.Scan(&newID)
	if err != nil {
		return -1, err
	}

	_, error := AfterOrderEvent(newID, app.Statuses.Given)
	if error != nil {
		log.Println(`Возникла ошибка после создания заказа `, error.Error())
	}

	return newID, nil
}

// AfterOrderEvent - событие которое вызываем после операций заказа
func AfterOrderEvent(lastId int64, status uint) (int64, error) {
	ctx := context.Background()
	var err error

	tsql := fmt.Sprintf(`INSERT INTO OLLIS_DELIVERY_STATUSES (
	OrderRowID
	,StatusCourier
	,CreatedAt
	,UpdatedAt
	) VALUES (
	%d
	,%d
	,'%s'
	,'%s'); select convert(bigint, SCOPE_IDENTITY());`,
		lastId,
		status,
		time.Now().Format("20060102 15:04:05.999999999Z07:00"),
		time.Now().Format("20060102 15:04:05.999999999Z07:00"))

	stmt, err := ConfMssql.DB.Prepare(tsql)
	log.Println(tsql)
	if err != nil {
		return -1, err
	}
	defer stmt.Close()

	row := stmt.QueryRowContext(ctx)
	var newID int64
	err = row.Scan(&newID)
	if err != nil {
		return -1, err
	}

	return newID, nil
}

// CreateMssqlOrder добавление заказа
func CreateMssqlUnit(unit *model.OllisDeliveryUnits) (int64, error) {
	ctx := context.Background()
	var err error

	tsql := fmt.Sprintf(`INSERT INTO OLLIS_DELIVERY_UNITS (
	GarageNumber
	,UnitID
	,DateEnd
	,DateStart
	,CreatedAt
	,UpdatedAt
	) VALUES (
	'%s'
	,%d
	,0
	,%d
	,'%s'
	,'%s'); select convert(bigint, SCOPE_IDENTITY());`,
		unit.GarageNumber,
		unit.UnitID,
		time.Now().Unix(),
		time.Now().Format("20060102 15:04:05.999999999Z07:00"),
		time.Now().Format("20060102 15:04:05.999999999Z07:00"))

	stmt, err := ConfMssql.DB.Prepare(tsql)
	log.Println(tsql)
	if err != nil {
		fmt.Println(err)
		return -1, err
	}
	defer stmt.Close()

	row := stmt.QueryRowContext(ctx)
	var newID int64
	err = row.Scan(&newID)
	if err != nil {
		fmt.Println(err)
		return -1, err
	}
	return newID, nil
}

func addCoordinates(points *model.Points) {
	for _, value := range points.Points {
		_, err := CreateMssqlGeo(&value)
		if err != nil {
			fmt.Println("Ошибка при добавлении координат", err)
		}
	}
}
func CreateMssqlGeo(geo *model.OllisDeliveryGeo) (int64, error) {
	ctx := context.Background()
	var err error

	tsql := fmt.Sprintf(`INSERT INTO OLLIS_DELIVERY_GEO (
	GarageNumber
	,Lat
	,Lng
	,Time
	,CreatedAt
	,UpdatedAt
	) VALUES (
	'%s'
	,%f
	,%f
	,%d
	,'%s'
	,'%s'); select convert(bigint, SCOPE_IDENTITY());`,
		geo.GarageNumber,
		geo.Lat,
		geo.Lng,
		geo.Time,
		time.Now().Format("20060102 15:04:05.999999999Z07:00"),
		time.Now().Format("20060102 15:04:05.999999999Z07:00"))

	stmt, err := ConfMssql.DB.Prepare(tsql)
	log.Println(tsql)
	if err != nil {
		fmt.Println(err)
		return -1, err
	}
	defer stmt.Close()

	row := stmt.QueryRowContext(ctx)
	var newID int64
	err = row.Scan(&newID)
	if err != nil {
		fmt.Println(err)
		return -1, err
	}
	return newID, nil
}

func main() {
	ConfMssql.SetConnectVars()
	ConfMssql.Initialize()

	Conf.SetConnectVars()
	Conf.Router = mux.NewRouter()
	Conf.Initialize()
	InitializeRoutes(&Conf)
	Conf.Run(`:` + app.GetPort())
}
