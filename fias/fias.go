package fias

type Geo struct {
	Id         int     `json:"id"`
	Guid       string  `json:"guid"`
	ObjectGUID string  `json:"objectGUID"`
	Name       string  `json:"name"`
	ManualName string  `json:"manualName"`
	House      string  `json:"house"`
	Est_status int     `json:"est_status"`
	Housing    string  `json:"housing"`
	Str_num    string  `json:"str_num"`
	Str_status int     `json:"str_status"`
	Lat        float64 `json:"lat"`
	Lng        float64 `json:"lng"`
	State      int     `json:"state"`
	Modified   string  `json:"modified"`
}

func getAddress(i interface{}) {

}
