package app

import (
	"testing"
)

func TestSetConnectVars(t *testing.T) {
	var app Config
	err := app.SetConnectVars()
	if err != nil {
		t.Error("не удоалось установить переменные")
	}
}

// TestConnection - тестирование коннекта к базе данных
func TestConnection(t *testing.T) {
	t.Skip("Пропуск теста подключения, так как пока нет возможности подключиться к хосту")
	conf := new(Config)
	conf.SetConnectVars()
	err := conf.Initialize()
	if err != nil {
		t.Errorf("При попытке инициализации подключения к базе произошла ошибка: %s", err.Error())
	}

	err = conf.DB.DB().Ping()
	if err != nil {
		t.Errorf("Ошибка подключения. Не найден сервер: %s", err.Error())
	}
}

// Тестирование провального коннекта к базе
func TestFailConnection(t *testing.T) {
	conf := new(Config)
	conf.SetConnectVars()
	conf.Dbpb.Server = `FAIL`
	err := conf.Initialize()
	if err != nil {
		t.Log([]string{"При попытке инициализации подключения к базе произошла ошибка: %s"}, err.Error())
	}

	err = conf.DB.DB().Ping()
	if err == nil {
		t.Errorf("Не удалось провалить подключение")
	}
}

// Установка значения по-умолчанию
func TestSetEnvDefault(t *testing.T) {
	// переменная по-умолчанию, котору будем ловить
	def := `defVal`

	catchEnv := getEnvFromKey(`NOTEXISTINGENV`, `defVal`)

	if def != catchEnv {
		t.Errorf(`Не удалось получить дефолтное значение`)
	}
}

func TestSetAllEnvDefault(t *testing.T) {
	conf := new(Config)
	conf.SetConnectVars()

	if len(conf.Dbpb.User) < 1 {
		t.Errorf(`Не инициализирована переменная conf.Dbpb.User`)
	}
	if len(conf.Dbpb.Pass) < 1 {
		t.Errorf(`Не инициализирована переменная conf.Dbpb.Pass`)
	}
	if len(conf.Dbpb.Server) < 1 {
		t.Errorf(`Не инициализирована переменная conf.Dbpb.Server`)
	}
	if len(conf.Dbpb.DbName) < 1 {
		t.Errorf(`Не инициализирована переменная conf.Dbpb.DbName`)
	}
	if conf.Dbpb.Port < 1 {
		t.Errorf(`Не инициализирована переменная conf.Dbpb.Port`)
	}
}
