// 17.10.2019 15:40 - Ollis Club
// Author: Spirichev Nikita <n.spirichev@ollis.ru>
//
// Основной параметры программы для управления логики курьера
//

package app

// statuses - структура статусов курьеров
type statuses struct {
	Given, OneTheRoad, InAddress, Canceled uint
}

// Расставим статусы по-умолчанию
var Statuses = statuses{25, 26, 27, 28}

const (
	// DistanceRestoran - Удалённость курьера от ресторана
	distanceRestoran = 100
	// DistanceClient - удалённость курьера от клиента
	distanceClient = 100
)

func GetDistanceResoran() float64 {
	return distanceRestoran
}

func GetDistanceClient() float64 {
	return distanceClient
}
